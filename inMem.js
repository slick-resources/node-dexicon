




module.exports = {

    db:{},
    
    state:{
      
      isInit:false  
        
        
    },
    
    tableNames:[],
    
    tables:{
      
      wordsspecial:false,
      
      semlinksspecial:false,
      
      samplesspecial:false,
      
      statisticsspecial:false
      
        
    },

    init:function(dbName, sqliteDb, tableNames)
    {
        
        console.log("Table names:" + JSON.stringify(tableNames));
        
       var loki = require('lokijs');
    
    this.db = new loki(dbName);
    
    var db = this.db;
    
    this.tableNames = tableNames;
    
    module.exports.tableNames = tableNames;
    
    this.db.loadDatabase({}, function(){
       
      var tables = module.exports.tables;
      
      var tableNames = module.exports.tableNames;
      
       for (var x = 0; x < tableNames.length; x++) {
        
        console.log("Table name:" + tableNames[x]);
        
        
        var tableName = tableNames[x];
            

                             module.exports.addTable(sqliteDb, tableName);
        
       
    }
      
      
    });
    
                
    
    }
    ,
    
    addTable:function(sqliteDb, tableName)
    {
        
        var tables = module.exports.tables;
        
                    sqliteDb.all('SELECT * FROM '+ tableName +'', function(err, rows) {

                              if (err) {

                                      return  console.log(err);

                              }
                              
                              var lastTableIX =  module.exports.tableNames.length - 1; 
                              
                              var lastRowIX = rows.length - 1;
                              
                              tables[tableName] = module.exports.db.addCollection(tableName);
                              
                                 for (var x = 0; x < rows.length; x++) {
                                    
                                     
                                           module.exports.insertToTable(tableName, rows[x]);
                                        
                                    
                                  
                                    
            
                           
            
        }
        
         console.log(tableName + ":loaded");
      
        if(tableName === module.exports.tableNames[lastTableIX])
        {
             console.log(tableName + ": all tables loaded");
            
        }
        
                              
                    });
                
        
     
        
    },
    
    
    
    insertToTable:function(tableName, insertObject, completeCallback)
    {
    
    var collection = this.tables[tableName];
    
    collection.insert(insertObject);
    
    if(typeof(completeCallback) === 'function')
    {
    
    completeCallback(false, collection);
    
    
    }
    
    
    
    }
  

}