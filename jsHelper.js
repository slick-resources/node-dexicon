module.exports =  {

    tick: 0,
    
    Image :function(src)
{
    
    var x = document ? document.createElement("IMG") : false;
    
    if (src  && src !== "") {
        
        x.src = src;
        
        //code
    }
    
   return x;
    
},

    logOnInterval: function(message, div, loggingOn) {

        this.tick += 1;

        if (div >= 0 && loggingOn && this.tick % div === 0) {

            console.log(message);

        }

    }
    ,
    
    
     
    isNumeric: function (n) {
  return n !== undefined && !isNaN(parseFloat(n)) && isFinite(n);
},

 
    isListAllNumeric:function(list)
    {
        var passed = typeof(list) === 'object';
        
        for(var x = 0; x < list.length; x++){
            
            if ( !this.isNumeric(list[x])) {
                
                passed = false;
                
            }
            
            
        }
        
        
        return passed;
         
    },
    
    
    
    

    isNonEmptyString:function(item){
        
        
        return item && item !== "";
       
    },
    
    
   
    
    
    
    isObject: function(item) {

        return item && typeof(item) === 'object';

    },

    isFunc: function(item) {

        return item && typeof(item) === 'function';

    },
    
    
    isArray:function(objects)
    {
        
        return  objects  instanceof Array;
        
        
    }
   
  
    
    ,
    
    
    findInArray:function(needle, haystack)
    {
        
        var ix = haystack.indexOf(needle);
        
        
        return ix >= 0 ?  haystack[ix] : false;
        
       
        
    },
    
    
      findInObj:function(needle, haystack)
    {
        
       for(var x in haystack)
       {
        
        if (haystack[x] === needle) {
            
            return needle;
            
            
        }
        
        
       }
       
        return false;
        
    }
    
    
    ,
    
      removeFromArray:function(needle, haystack)
    {
      
      
      var x = haystack.indexOf(needle);
      
      if (x >= 0) {
        
        haystack.splice(x, 1);
        
        
      }
        
        
    },
    
  
   
   
   
    
 extend: function(destination, source) {
        for (var property in source) {
            if (typeof source[property] === "object" && source[property] !== null && destination[property]) { 
                this.extend(destination[property], source[property]);
            } else {
                destination[property] = source[property];
            }
        }
        
        return destination;
        
    }
  ,
    
    
    //Extend group: for adding stateless deps
    
 extendGroup: function(destination, sourceSet) {
    
     for (var ssx in sourceSet) {
    
    var source = sourceSet[ssx];
    
        for (var property in source) {
            if (typeof source[property] === "object" && source[property] !== null && destination[property]) { 
                this.extend(destination[property], source[property]);
            } else {
                destination[property] = source[property];
            }
        }
        
        }
        
        return destination;
        
    }
  
    

    
};



